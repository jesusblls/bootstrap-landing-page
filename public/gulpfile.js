var gulp = require('gulp');
var mincss = require('gulp-clean-css');
var mincss2 = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');
var minjs = require('gulp-uglify');
var runSequence = require('run-sequence');

gulp.task('minificar-css', function(){
  gulp.src('src/css/*.css')
    .pipe( mincss())
    .pipe( mincss2())
    .pipe( gulp.dest('public/css'));
});

gulp.task('minificar-img', function(){
  gulp.src('src/css/*')
    .pipe( imagemin())
    .pipe( gulp.dest('public/images'));
});

gulp.task('minificar-js', function(){
  gulp.src('src/js/*.js')
    .pipe( minjs())
    .pipe( gulp.dest('public/js'));
});

var browserSync = require('browser-sync')
gulp.task('browser-sync', function () {
  var config = {
    startPath: 'index.html',
    notify: false,
    server: {
      baseDir: 'public/'
    }
  };
  browserSync.init(config);
});

gulp.task('browser-reload', function(){
  browserSync.reload();
});

//workflow: $ gulp
//1.
gulp.task('default', function () {
  runSequence(
    ['browser-sync'],
    ['minificar-img'],
    ['minificar-css'],
    ['minificar-js']
  );

  gulp.watch(['src/*', 'scr/**/*'], function() {
    runSequence(
      ['minificar-img'],
      ['minificar-css'],
      ['minificar-js'],
      ['browser-reload']
    );
  });
})
